<?php

namespace Drupal\postfile\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\File\Exception\FileException;
use Drupal\Core\File\Event\FileUploadSanitizeNameEvent;
use Drupal\Core\File\FileExists;
use Drupal\Core\File\FileSystemInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * File Controller class.
 */
class File extends ControllerBase {

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected EventDispatcherInterface $eventDispatcher;

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected FileSystemInterface $fileSystem;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected RequestStack $requestStack;

  /**
   * Constructs a FileSystemForm object.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   */
  public function __construct(RequestStack $request_stack, FileSystemInterface $file_system, EventDispatcherInterface $event_dispatcher) {
    $this->requestStack = $request_stack;
    $this->fileSystem = $file_system;
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack'),
      $container->get('file_system'),
      $container->get('event_dispatcher')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function upload() {
    if (!$this->currentUser()->isAnonymous() && $this->currentUser()->hasPermission('postfile upload')) {
      /** @var \Symfony\Component\HttpFoundation\Request $request */
      $request = $this->requestStack->getCurrentRequest();
      /** @var \Drupal\Core\Config\Config $config */
      $config = $this->config('postfile.settings');
      /** @var \Symfony\Component\HttpFoundation\File\UploadedFile $uploaded_file */
      $uploaded_file = $request->files->get('file');

      // Sanitize the file name.
      $file_name = basename($uploaded_file->getClientOriginalName());
      // Allowed extensions should never be NULL, but just in case.
      $allowed_extensions = $config->get('allowed_extensions') ?? '';
      $event = new FileUploadSanitizeNameEvent($file_name, $allowed_extensions);
      $this->eventDispatcher->dispatch($event);
      $file_name = $event->getFilename();

      // Prepare variables.
      $directory = $config->get('filesystem') . '://' . $config->get('directory');
      $destination = $directory . '/' . $file_name;

      // Test file extension.
      $file_extension_error = FALSE;
      if (!$this->config('system.file')->get('allow_insecure_uploads') && preg_match(FileSystemInterface::INSECURE_EXTENSION_REGEX, $file_name)) {
        $file_extension_error = TRUE;
      }
      if (empty($allowed_extensions) || !preg_match('/\.(' . implode('|', explode(' ', $allowed_extensions)) . ')$/i', $file_name)) {
        $file_extension_error = TRUE;
      }
      if ($file_extension_error) {
        return new JsonResponse(['message' => 'There was an error uploading the file - invalid file extension.'], 500);
      }

      // Handle the physical file.
      if ($this->fileSystem->prepareDirectory($directory, FileSystemInterface:: CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS)) {
        try {
          switch ($config->get('exists_action')) {
            case 'replace':
              $file_exists = FileExists::Replace;
              break;
            case 'error':
              $file_exists = FileExists::Error;
              break;
            case 'rename':
            default:
              $file_exists = FileExists::Rename;
              break;
          }

          $destination = $this->fileSystem->copy($uploaded_file->getRealPath(), $destination, $file_exists);

          /** @var \Drupal\file\Entity\File $file */
          $file = $this->entityTypeManager()
            ->getStorage('file')
            ->create(
              [
                'filename' => $file_name,
                'uri' => $destination,
                'status' => 1,
                'uid' => $this->currentUser()->id(),
              ]
            );
          $file->save();

          return new JsonResponse(['message' => 'Successfully uploaded the file - ' . $destination], 201);
        }
        catch (FileException $e) {
          return new JsonResponse(['message' => 'There was an error uploading the file - ' . $e->getMessage()], 500);
        }
      }
    }

    return new JsonResponse(['message' => 'There was an error preparing the directory for file upload.'], 500);
  }

}
