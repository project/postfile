<?php

namespace Drupal\postfile\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StreamWrapper\StreamWrapperInterface;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Settings Form class.
 */
class Settings extends ConfigFormBase {

  /**
   * The stream wrapper manager.
   *
   * @var \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface
   */
  protected StreamWrapperManagerInterface $streamWrapperManager;

  /**
   * Constructs a FileSystemForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typedConfigManager
   *   The typed config manager.
   * @param \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface $stream_wrapper_manager
   *   The stream wrapper manager.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    TypedConfigManagerInterface $typedConfigManager,
    StreamWrapperManagerInterface $stream_wrapper_manager,
  ) {
    parent::__construct($config_factory, $typedConfigManager);
    $this->streamWrapperManager = $stream_wrapper_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('stream_wrapper_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'postfile_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'postfile.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $settings = $this->config('postfile.settings');

    $form['filesystem'] = [
      '#type' => 'radios',
      '#title' => $this->t('File System'),
      '#default_value' => $settings->get('filesystem'),
      '#options' => $this->streamWrapperManager->getDescriptions(StreamWrapperInterface::WRITE_VISIBLE),
    ];

    $form['directory'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Directory'),
      '#description' => $this->t('The directory within the filesystem to store the file. Do not include the beginning or ending slashes.'),
      '#default_value' => $settings->get('directory'),
    ];

    $form['exists_action'] = [
      '#type' => 'radios',
      '#title' => $this->t('File Exists Action'),
      '#description' => $this->t('If the file exists, which action should be taken.'),
      '#default_value' => $settings->get('exists_action'),
      '#options' => [
        'rename' => $this->t('Rename file'),
        'replace' => $this->t('Replace file'),
        'error' => $this->t('Throw error'),
      ],
    ];

    $form['allowed_extensions'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Allowed file extensions'),
      '#description' => $this->t('Separate extensions with a comma or space. Each extension can contain alphanumeric characters, ".", and "_", and should start and end with an alphanumeric character.'),
      '#default_value' => str_replace(' ', ', ', $settings->get('allowed_extensions')),
      '#weight' => 1,
      '#maxlength' => 256,
      '#required' => TRUE,
      '#element_validate' => [['Drupal\file\Plugin\Field\FieldType\FileItem', 'validateExtensions']],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $this->config('postfile.settings')
      ->set('filesystem', $values['filesystem'])
      ->set('directory', $values['directory'])
      ->set('exists_action', $values['exists_action'])
      ->set('allowed_extensions', $values['allowed_extensions'])
      ->save();

    parent::submitForm($form, $form_state);
  }

}
