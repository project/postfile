CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Usage

INTRODUCTION
------------

POST File was created because there was no obvious way to upload a managed file
using the REST API or the JSONAPI. This module solves that problem by creating
an endpoint that allows a privileged user to upload a file to a specific file
system (public or private) without attaching that file to any entity. This file
can then be processed by Feeds for example.

REQUIREMENTS
------------

The endpoint requires a valid user before is allows the file to be uploaded and
saved to the file system. It can use any of the REST authentication providers
but is currently only configured to use basic_auth.

INSTALLATION
------------

Install as you would normally install a contributed Drupal module. Visit:
https://www.drupal.org/documentation/install/modules-themes/modules-8 for
further information.

CONFIGURATION
-------------

* Configure user permissions in Administration » People » Permissions:

  - Manage POST File settings

    This permission controls who can modify the settings of the module.

  - Upload file

    This permissions controls who can upload a file.

* Customize the module settings in Administration » Configuration » Media
    » POST File Settings

USAGE
-----

The module creates an endpoint on the site at /postfile/upload that accepts POST
requests. Using the basic_auth module, the request would look something like
this:

```
curl
--location 'https://www.example.com/postfile/upload' \
--header 'Authorization: Basic dXNlcm5hbWU6cGFzc3dvcmQ' \
--form 'file=@"/path/to/file.csv"'
```
